<?php

namespace Core;

use PDO;
use App\Config;

/**
 * Base model
 */

abstract class Model
{
    /**
     * Get the PDO connection
     */
    protected static function getDB()
    {
        static $db = null;

        if($db === null) {

            try {
                $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' .
                    Config::DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);

                // Throw an exception when an error occurs
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                return $db;
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
        return $db;
    }

    protected static function validate($string, $method = [])
    {
        foreach ($method as $key => $value)
        {
            switch ($key) {
                case 'text':
                    if (!preg_match("/^[a-zA-Zа-яА-Я]*$/", $string)) {
                        return false;
                    }
                    break;
                case 'email':
                    if (!filter_var($string, FILTER_VALIDATE_EMAIL)) {
                        return false;
                    }
                    break;
                case 'maxlen':
                    if (strlen($string) > $value) {
                        return false;
                    }
                    break;
                case 'min':
                    if ($string < $value) {
                        return false;
                    }
                    break;
                case 'max':
                    if ($string > $value) {
                        return false;
                    }
                    break;
                case 'number':
                    if (!preg_match("/^[\d]*$/", $string)) {
                        return false;
                    }
                    break;
            }
        }
        return true;
    }
}