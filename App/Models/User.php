<?php

namespace App\Models;

use PDO;

/**
 * Post model
 */

class User extends \Core\Model
{
    /*
     * Get all the posts as an associative array
     */
    public static function getAll($start = 0, $perPage = 10)
    {
        try {
            $db = static::getDB();

            $stmt = $db->prepare('SELECT SQL_CALC_FOUND_ROWS id, firstname, secname, year, email, password FROM users
                                WHERE usergroup = 1
                                ORDER BY `id` LIMIT ?, ?');
            $stmt->bindParam(1, $start, PDO::PARAM_INT);
            $stmt->bindParam(2, $perPage, PDO::PARAM_INT);

            $stmt->execute();

            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = $db->query('SELECT FOUND_ROWS()');
            $pagesCount = $stmt->fetch(PDO::FETCH_ASSOC);
            $pagesCount = ceil($pagesCount['FOUND_ROWS()'] / $perPage);

            $results['_pagesCount'] = $pagesCount;

            return $results;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function add($name, $secname, $email, $year, $pass)
    {
        $errors = [];
        try {
            $db = static::getDB();

            $stmt = $db->prepare('SELECT id FROM users WHERE email = :email');

            $stmt->execute(array(':email' => $email));

            if ($user = $stmt->fetch(PDO::FETCH_LAZY)) {
                $errors[] = "Пользователь с таким имейлом уже зарегистрирован.";
                return $errors;
            }
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
        try {
            $db = static::getDB();

            if (!User::validate($name, array('text' => ''))) {
                $errors[] = "Поле 'Имя' должно содержать только буквы.";
            }
            if (!User::validate($secname, array('text' => ''))) {
                $errors[] = "Поле 'Фамилия' должно содержать только буквы.";
            }
            if (!User::validate($email, array('email' => ''))) {
                $errors[] = "Поле 'Email' должно содержать корректный адрес электронной почты.";
            }
            if (!User::validate($year, array('number' => '', 'maxlen' => '4', 'min' => 1950, 'max' => 2015))) {
                $errors[] = "Поле 'Год рождения' должно содержать число 1950-2015.";
            }

            if (count($errors) > 0) {
                return $errors;
            }

            $stmt = $db->prepare('INSERT INTO `users` (
                                                `firstname`,
                                                `secname`,
                                                `year`,
                                                `email`,
                                                `password`)
                                    VALUES (
                                                :name,
                                                :sname,
                                                :year,
                                                :email,
                                                :pass
                                    )');
            $stmt->execute(array(':name' => $name,
                                ':sname' => $secname,
                                ':year' => $year,
                                ':email' => $email,
                                ':pass' => md5(md5($pass))
                ));

            return $errors;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function isUserCorrect($email, $userpass)
    {
        try {
            $db = static::getDB();

            $stmt = $db->prepare('SELECT password FROM users
                              WHERE email = :email');
            $stmt->execute(array(':email' => $email));

            if ($user = $stmt->fetch(PDO::FETCH_LAZY)) {
                if (md5(md5($userpass)) == $user['password']) {
                    return true;
                }
            }
            return false;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getUser($email)
    {
        try {
            $db = static::getDB();

            $stmt = $db->prepare('SELECT id, firstname, secname, year, email, password, usergroup
                              FROM users
                              WHERE email = :email');
            $stmt->execute(array(':email' => $email));

            if ($user = $stmt->fetch(PDO::FETCH_LAZY)) {
                return $user;
            }
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getUserByID($id)
    {
        try {
            $db = static::getDB();

            $stmt = $db->prepare('SELECT id, firstname, secname, year, email, password, usergroup
                              FROM users
                              WHERE id = :id');
            $stmt->execute(array(':id' => $id));

            if ($user = $stmt->fetch(PDO::FETCH_LAZY)) {
                return $user;
            }
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function delete($id)
    {
        try {
            $db = static::getDB();

            $stmt = $db->prepare('DELETE FROM users WHERE id = ?');
            $stmt->bindParam(1, $id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (\PDOException $e) {
            $e->getMessage();
        }
    }

    public static function updateUser($id, $params)
    {
        try {
            $db = static::getDB();

            $stmt = $db->prepare('UPDATE users
                                    SET
                                      firstname = :name,
                                      secname = :sname,
                                      email = :email,
                                      year = :year,
                                      password = :pass
                                    WHERE id = :id'
            );

            $stmt->execute(array(
                            ':name' => $params['name'],
                            ':sname' => $params['sname'],
                            ':email' => $params['email'],
                            ':year' => $params['year'],
                            ':pass' => md5(md5($params['pass'])),
                            ':id' => $id
            ));

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }
}