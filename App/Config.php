<?php

namespace App;

/**
 * App configuration
 */

class Config
{
    const DB_HOST = 'localhost';
    const DB_NAME = 'mysite'; // f90810c4_mysite
    const DB_USER = 'root'; // f90810c4_mysite
    const DB_PASSWORD = ''; // 8p329c7G

    // true for show errors, false for hiding errors and write them to a log file
    const SHOW_ERRORS = true;
}