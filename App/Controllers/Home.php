<?php

namespace App\Controllers;

use Core\View;

/**
 * Home controller
 */

class Home extends \Core\Controller
{
    protected function before()
    {
        //echo "(before) ";
        //return false;
    }

    protected function after()
    {
        //echo " (after)";
    }

    /**
     * Show the index page
     */
    public function indexAction()
    {
        //echo "Hello from the index action in the Home controller!";

        header("Location: users/index");

        View::renderTemplate('Home/index.html', [
            'name'      => 'John',
            'colours'   => ['red', 'yellow', 'blue']
        ]);
    }
}