<?php

namespace App\Controllers\Admin;

use \Core\View;
use App\Models\User;

/**
 * Users admin controller
 */

class Users extends \Core\Controller
{
    // make sure an admin user is logged in
    protected function before()
    {
        if (isset($_SESSION['usergroup'])) {
            if ($_SESSION['usergroup'] == 2) {
                return true;
            }
        }
        header('Location: /');
    }

    public function indexAction()
    {
        $perPage = 10;
        $curPage = 1;

        if (isset($_GET['page']) && $_GET['page'] > 0) {
            $curPage = $_GET['page'];
        }
        $start = ($curPage - 1) * $perPage;

        $users = User::GetAll($start, $perPage);
        $pagesCount = array_pop($users);

        View::renderTemplate('admin/Users/index.html', [
            'users' => $users,
            'pagesCount' => $pagesCount
        ]);
    }

    public function viewAction()
    {
        if (isset($this->route_params['id'])) {

            $user = User::getUserByID($this->route_params['id']);

            View::renderTemplate('admin/Users/view.html', [
                'user' => $user
            ]);
        }
    }

    public function deleteAction()
    {
        if (isset($this->route_params['id'])) {
            User::delete($this->route_params['id']);
        }
        header('Location: /');
    }

    public function editAction()
    {
        if (isset($this->route_params['id'])) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $params['name'] = htmlspecialchars($_POST['name']);
                $params['sname'] = htmlspecialchars($_POST['secname']);
                $params['email'] = htmlspecialchars($_POST['email']);
                $params['year'] = htmlspecialchars($_POST['year']);
                $params['pass'] = htmlspecialchars($_POST['pass']);

                User::updateUser($this->route_params['id'], $params);

                header('Location: /');
            } else {

                $user = User::getUserByID($this->route_params['id']);

                View::renderTemplate('admin/Users/edit.html', [
                    'id' => $this->route_params['id'],
                    'user' => $user
                ]);
            }
        } else {
            header('Location: /');
        }
    }

}