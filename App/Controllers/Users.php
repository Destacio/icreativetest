<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;

class Users extends \Core\Controller
{
    public function indexAction()
    {
        if ( isset($_SESSION['username'])) {
            if ($_SESSION['usergroup'] == 2) {
                header('Location: /admin/users/index?page=1');
            }
            else {
                View::renderTemplate('Users/index.html', [
                    'username' => $_SESSION['username']
                ]);
            }
        }
        else {
            View::renderTemplate('Users/login.html');
        }
    }

    public function signupAction()
    {
        if ( $_SERVER['REQUEST_METHOD'] == "POST") {
            $name = htmlspecialchars($_POST['name']);
            $sname = htmlspecialchars($_POST['secname']);
            $email = htmlspecialchars($_POST['email']);
            $year = htmlspecialchars($_POST['year']);
            $pass = htmlspecialchars($_POST['pass']);

            $errors = User::add($name, $sname, $email, $year, $pass);

            if (count($errors) == 0) {
                header("Location: index");
            }
            else {
                View::renderTemplate('Users/signup.html', [
                    'name' => $name,
                    'sname' => $sname,
                    'email' => $email,
                    'year' => $year,
                    'errors' => $errors
                ]);
            }

        }
        else {
            View::renderTemplate('Users/signup.html');
        }
    }

    public function loginAction()
    {
        if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {
            $email = htmlspecialchars($_POST['email']);
            $pass = htmlspecialchars($_POST['userpass']);

            if (User::isUserCorrect($email, $pass)) {
                $user = User::getUser($email);

                $_SESSION['username'] = $user['firstname'];
                $_SESSION['usergroup'] = $user['usergroup'];
            }
        }
        header("Location: index");
    }

    public function logoutAction()
    {
        if (isset($_SESSION['username'])) {
            unset($_SESSION['username']);
            unset($_SESSION['usergroup']);
        }
        header("Location: index");
    }
}