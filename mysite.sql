-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 24 2016 г., 19:24
-- Версия сервера: 5.5.48
-- Версия PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mysite`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `secname` varchar(128) NOT NULL,
  `year` int(4) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usergroup` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `firstname`, `secname`, `year`, `email`, `password`, `usergroup`) VALUES
(1, 'Dest', 'Jey', 1992, 'dest@mail.com', 'ec6a6536ca304edf844d1d248a4f08dc', 2),
(2, 'Вася', 'Пупкин', 1997, 'vasya@pup.kin', '3049a1f0f1c808cdaa4fbed0e01649b1', 1),
(3, 'John', 'Smith', 1991, 'john@smith.com', '74be16979710d4c4e7c6647856088456', 1),
(4, 'Fname', 'Sname', 0, 'email@mail.com', '1234', 1),
(7, '1234', '1234', 0, '12@212.2', '1245', 1),
(8, '123', '123', 1992, '12@12f', '9d0dbc1918c080d7f4a65c2d0b2bda1f', 1),
(10, 'qqqq', 'qqqqqq', 0, 'qqq@qqq', 'qqq', 1),
(11, 'aaa', 'aaa', 1950, 'aaa@aaa', 'aaa', 1),
(12, 'aaa', 'aaa', 0, 'aaa@aaa', 'aaa', 1),
(13, 'aaa', 'aaa', 0, 'aaa@aaa', 'aaa', 1),
(14, 'aaa', 'aaa', 1900, 'aaa@aaa', 'aaa', 1),
(15, 'aaa', 'aaa', 1950, 'aaa@aaa', 'aaa', 1),
(16, 'bbb', 'bbb', 1950, 'bbb@bbb', 'bbb', 1),
(18, 'ddd', 'ddd', 1950, 'ddd@ddd', '1d8922d005309356634c3114859436f2', 1),
(19, 'eee', 'eee', 1951, 'eee@eee', '66ca1f8a07c26dca7bd7b07390a29238', 1),
(20, 'fff', 'fff', 1953, 'fff@fff', 'b8d371756f63a8572258db2e20bf52e6', 1),
(21, 'www', 'www', 1952, 'www@www', '985bda1a6bf60cbb8960d0397c9b9d39', 1),
(23, 'OneMoreUser', 'Clone', 1950, 'another@mail.1', '28c8edde3d61a0411511d3b1866f0636', 1),
(24, 'OneMoreUser', 'Clone', 1950, 'another@mail.2', '665f644e43731ff9db3d341da5c827e1', 1),
(25, 'OneMoreUser', 'Clone', 1950, 'another@mail.3', '38026ed22fc1a91d92b5d2ef93540f20', 1),
(27, 'OneMoreUser', 'Clone', 1951, 'another@mail.ru', 'ec6a6536ca304edf844d1d248a4f08dc', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
